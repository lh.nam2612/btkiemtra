﻿using System.ComponentModel.DataAnnotations;

namespace BtKiemTra.Models
{
    public class Logs
    {
        [Key]
        public int LogsID { get; set; }
        public int TransactionalID { get; set; }
        public Transactions Transactions { get; set; }
        [Required]

        public DateTime Date {  get; set; }
        public DateTime Time { get; set; }
    }
}
