﻿using System.ComponentModel.DataAnnotations;

namespace BtKiemTra.Models
{
    public class Customer
    {
       [Key]
        public int CustomerID { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Contact { get; set; }

        public string Address { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }

    }
}
