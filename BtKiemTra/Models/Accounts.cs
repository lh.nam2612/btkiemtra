﻿using System.ComponentModel.DataAnnotations;

namespace BtKiemTra.Models
{
    public class Accounts
    {
        [Key]
        public int  AccountID { get; set; }
        [Required]
        public int CustomerID { get; set; } 
        public Customer? Customer { get; set; }
        [Required]
        public string AccountName { get; set; }
    }
}
