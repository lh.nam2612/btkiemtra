﻿using System.ComponentModel.DataAnnotations;

namespace BtKiemTra.Models
{
    public class Reports
    {
        [Key]
        public int ReportID { get; set; }
        public int AccountID { get; set;}
        public Accounts Accounts { get; set; }
        [Required]
        public int LogsID {  get; set;}

        public int TransactionalID {  get; set;}
        public Transactions Transactions { get; set; }
        [Required]
        public string Reportname { get; set;}

        public string Reportdate {  get; set;}

    }
}
