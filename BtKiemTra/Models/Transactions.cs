﻿using System.ComponentModel.DataAnnotations;

namespace BtKiemTra.Models
{
    public class Transactions
    {
        [Key]
        public int TransactionID { get; set; }


        public int EmployeesID { get; set; }
        public Employees Employees { get; set; }
        [Required]
        public int CustomerID { get; set; }

        public Customer Customer { get; set; }
        [Required]
        
        public string Name { get; set; }
    }
}
